import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DataserviceService } from '../dataservice.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title = 'Azure Kubernetes Service: Sample Angular App';
  public async: any;
  messageSignalR: string;
  messageSignalRFunc: string;
  messages: string[] = [];
  Users: Observable<string[]>;
  errors: string[] = [];

  constructor(private dataService: DataserviceService) {
  }

  ngOnInit() {
    
  }


  getUsers() {
    this.Users = this.dataService.getUsers();
  }


}
